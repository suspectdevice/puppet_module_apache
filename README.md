**DEMO**

screencast (video):
https://www.screencast.com/t/vFEFA0iaXM4

demo log: 
https://gitlab.com/suspectdevice/puppet_module_apache/blob/master/demo_console_out.log

**Purpose:**

This project is comprised of a puppet module for Apache2 that may be used with Debian and RedHat based nodes. 

It also includes a script that will test the module as follows: 

 1. launch a Docker image containing a pre-configured puppet server that mounts host-based module project to the appropriate directories within the container
 2. launch a Docker image containing a pre-configured puppet agent in Ubuntu 16.04 and trigger an agent module refresh to install Apache2 to the node
 3. launch a Docker image containing a pre-configured puppet agent in CentOS 7 and trigger an agent module refresh to install Apache2 to the node
 4. test that apache2 is running test.example.com custom index.html page on the Ubuntu instance
 5. test that the redirect from test.example.com/test to test.example.com/ is working on the Ubuntu instance
 6. test that apache2 is running test.example.com custom index.html page on the CentOS instance
 7. test that the redirect from test.example.com/test to test.example.com/ is working on the CentOS instance
 8. tear all test instances/resources down
 

**Prerequisites:**

The test script requires that:

 1. Docker 1.11+ is installed
 2. The test machine is not Windows (script does not support Windows path mounting conventions in Docker)
 3. Sudo access - this is necessary to create the /etc/hosts file entry for test.example.com
 

**Instructions:**

To run the test bash script, simply clone the project locally and execute the following command from project root:
> sudo ./test_apache_module.sh

all information about the run will be printed to the console


**Extras:**

This project also includes the custom Dockerfiles used to create the agent images. The images are pre-built and available in Dockerhub:

- nechtanhub/centos_agent
- nechtanhub/ubuntu_agent

They may be modified and updated in dockerhub via the following commands from each Dockerfile directory root:

> docker build . -t nechtanhub/centos_agent

> docker push nechtanhub/centos_agent





