node default {
  $adminemail = 'webmaster@example.com'
  $servername = 'test.example.com'

  include apache
  include apache::vhosts
}
