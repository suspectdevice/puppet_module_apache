class apache (
  $apachepkg    = $::apache::params::apachepkg,
  $configtarget  = $::apache::params::configtarget,
  $configsource = $::apache::params::configsource,
) inherits ::apache::params {

  package { 'apache':
      name    => $apachepkg,
      ensure  => present,
  }

  file { 'configuration-file':
      path => $configtarget,
      ensure => file,
      source => $configsource,
      notify => Service['apache-service'],
  }

}
