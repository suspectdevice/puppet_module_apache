class apache::params {

   case $::osfamily {

      'Debian': {
        $apachepkg = 'apache2'
        $configtarget = '/etc/apache2/apache2.conf'
        $configsource = 'puppet:///modules/apache/apache2.conf'
      }

      'RedHat': {
        $apachepkg = 'httpd'
        $configtarget = '/etc/httpd/conf/httpd.conf'
        $configsource = 'puppet:///modules/apache/httpd.conf'
      }

      default: {
         fail("Unsupported distribution. Cannot install Apache package.")
      }
   }
}
