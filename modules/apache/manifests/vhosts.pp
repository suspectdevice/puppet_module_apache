class apache::vhosts (
  $apachepkg    = $::apache::params::apachepkg,
) inherits ::apache::params {

  case $::osfamily {

      'Debian': {
          $vhostconf = "/etc/apache2/sites-available/$servername.conf"
          $vhosttemplate = 'apache/vhosts-deb.conf.erb'

          file { $vhostconf:
            ensure => file,
            content => template($vhosttemplate),
          }
          file { "/etc/apache2/sites-enabled/$servername.conf":
            require => File[$vhostconf],
            ensure => "link",
            target => $vhostconf,
          }
      }

      'RedHat': {
          $vhostconf = '/etc/httpd/conf.d/vhost.conf'
          $vhosttemplate = 'apache/vhosts-rh.conf.erb'

          file { $vhostconf:
            ensure => file,
            content => template($vhosttemplate),
          }
      }

      default: {
          fail("Unsupported distribution. Cannot configure Apache.")
      }
  }

  file { "/var/www/$servername":
    ensure => directory,
  }
  file { "/var/www/$servername/public_html":
    ensure => directory,
  }
  file { "/var/www/$servername/logs":
    ensure => directory,
  }
  file { "/var/www/$servername/public_html/index.html":
    ensure => file,
    content => file('apache/index.html'),
  }

  service { 'apache-service':
    name => $apachepkg,
    ensure  => running,
    enable  => true,
    hasrestart => true,
    subscribe => File[$vhostconf]
    # loglevel  => 'debug'
  }

}
