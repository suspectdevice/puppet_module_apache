#!/bin/bash
set -o verbose

docker stop puppet && docker rm puppet
docker stop puppet-agent-ubuntu && docker rm puppet-agent-ubuntu
docker stop puppet-agent-centos && docker rm puppet-agent-centos
docker network rm puppet