#!/bin/bash

echo creating docker puppet network...
docker network create puppet

basepath="$(pwd)"
serverpath='/etc/puppetlabs/code'

echo
echo running docker puppet server...
docker run -d --net puppet --name puppet --hostname puppet-server -v ${basepath}/modules/apache:${serverpath}/modules/apache -v ${basepath}/environments/production/manifests:${serverpath}/environments/production/manifests puppet/puppetserver-standalone

echo
echo verify docker puppet server still running:
docker ps

echo
echo sleep for a minute while puppet server starts up...
sleep 45

echo
echo run ubuntu agent and trigger apache2 module install, which should bind site to host port 8080...
docker run -d -t --net puppet --name puppet-agent-ubuntu --hostname puppet-agent-ubuntu -p 8080:80 nechtanhub/ubuntu_agent
docker exec -it puppet-agent-ubuntu puppet agent -t --verbose --no-daemonize --summarize

echo
echo run centos agent and trigger apache2 module install, which should bind site to host port 8081...
docker run -d -ti --privileged --net puppet --name puppet-agent-centos --hostname puppet-agent-centos -p 8081:80 -e "container=docker" -v /sys/fs/cgroup:/sys/fs/cgroup nechtanhub/centos_agent /usr/sbin/init
docker exec -it puppet-agent-centos puppet agent -t --verbose --no-daemonize --summarize

echo make sure all are up...
docker ps

echo
echo print all certs registered with server now, just for fun:
docker exec -it puppet puppet cert list -all

echo
echo make hosts file entry to resolve test.example.com to localhost IP:
matches_in_hosts="$(grep -n test.example.com /etc/hosts | cut -f1 -d:)"
if [ ! -z "$matches_in_hosts" ]
then
	echo skipping as entry aready exists
else
	echo "127.0.0.1  test.example.com" >> /etc/hosts
fi

echo
echo make sure apache running on Ubuntu at test.example.com:8080 via curl:
curl --retry 1 http://test.example.com:8080

echo
echo make sure apache is up and redirecting "/test" to root on Ubuntu at test.example.com:8080/test via curl:
curl --retry 1 http://test.example.com:8080/test

echo
echo make sure apache running on CentOS at test.example.com:8081 via curl:
curl --retry 1 http://test.example.com:8081

echo
echo make sure apache is up and redirecting "/test" to root on CentOS at test.example.com:8081/test via curl:
curl --retry 1 http://test.example.com:8081/test

echo
read -n 1 -s -p "Test complete. Press any key to tear it down."

set -o verbose

docker stop puppet && docker rm -v puppet
docker stop puppet-agent-ubuntu && docker rm -v puppet-agent-ubuntu
docker stop puppet-agent-centos && docker rm -v puppet-agent-centos
docker network rm puppet
